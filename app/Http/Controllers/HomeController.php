<?php

namespace App\Http\Controllers;

use App\States\Open;
use App\States\StatusTicket;
use App\Status;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function ticket()
    {
       return $statusConfig = config('status.status');
        $payment = Status::find(1);

       return $payment->active->transitionTo(Open::class);

        return view('ticket');
    }
}
