<?php

namespace App\States;
use App\States\TicketState;

class DevReview extends TicketState
{
    public function status()
    {
        return "DevReview";
    }

}
