<?php

namespace App\States;
use App\States\TicketState;

class InProgress extends TicketState
{
    public function status()
    {
        return "InProgress";
    }

}
