<?php

namespace App\States;


class ReOpened extends TicketState
{
    public function status()
    {
        return "ReOpened";
    }

}
