<?php

namespace App\States;


class Resolved extends TicketState
{
    public function status()
    {
        return "Resolved";
    }

}
