<?php

namespace App\States;
use Spatie\ModelStates\State;

abstract class TicketState extends State
{
    abstract public function status();

}
