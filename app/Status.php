<?php

namespace App;

use App\States\DevReview;
use App\States\InProgress;
use App\States\NeedTest;
use App\States\Open;
use App\States\ReOpened;
use App\States\Resolved;
use App\States\Testing;
use App\States\TicketState;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\ModelStates\HasStates;

class Status extends Model
{
    use Notifiable , HasStates;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
    ];

    /**
     * @property \App\States\TicketState $state
     */


    protected $table = "status";

    protected $active = [
        'active' => TicketState::class
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */



    protected function registerStates(): void
    {
        $this->addState('active' , TicketState::class)
            ->allowTransition(Open::class, InProgress::class)
            ->allowTransition(Open::class, NeedTest::class)
            ->allowTransition(InProgress::class, Open::class)
            ->allowTransition(InProgress::class, DevReview::class)
            ->allowTransition(DevReview::class, NeedTest::class)
            ->allowTransition(DevReview::class, Open::class)
            ->allowTransition(NeedTest::class, Testing::class)
            ->allowTransition(Testing::class, ReOpened::class)
            ->allowTransition(Testing::class, Resolved::class)
            ->allowTransition(ReOpened::class, NeedTest::class)
            ->allowTransition(ReOpened::class, InProgress::class);
//            ->allowTransition();
    }

}
